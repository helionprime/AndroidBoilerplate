package com.bajicdusko.androidboilerplate;

/**
 * Created by Bajic Dusko (www.bajicdusko.com) on 15-Aug-16.
 */

public interface Injector {
    BoilerplateDaggerComponent injector();
}
