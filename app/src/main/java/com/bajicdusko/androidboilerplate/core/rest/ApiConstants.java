package com.bajicdusko.androidboilerplate.core.rest;

import com.bajicdusko.androidboilerplate.BuildConfig;

/**
 * Created by Bajic Dusko (www.bajicdusko.com) on 15-Aug-16.
 */

public class ApiConstants {
    public static final String API_URL = BuildConfig.BASE_URL;
}
