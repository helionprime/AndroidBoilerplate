package com.bajicdusko.androidboilerplate.ui.fragment;

public interface IFragment {
    String getFragmentName();
}
